class Calculation {
	
	public void add(int x, int y) {
		System.out.println(x + y);
	}
	
	public void add(int x, int y, int z) {
		System.out.println(x + y + z);
	}
	
	public void add(int x, float y) {
		System.out.println(x + y);
	}

}

public class OverloadingMain {

	public static void main(String[] args) {
		
		Calculation obj = new Calculation();
		obj.add(5, 6);// methods are used for
		obj.add(7, 8);// reusability
		obj.add(5, 6.5f);
		obj.add(5, 6, 7);
	}

}
