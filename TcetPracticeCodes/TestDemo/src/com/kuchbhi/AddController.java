package com.kuchbhi;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import org.hibernate.Session;
import org.hibernate.cfg.Configuration;
import org.hibernate.query.Query;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

class Student {
	String name;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Student(String name) {
		super();
		this.name = name;
	}

	@Override
	public String toString() {
		return "Student [name=" + name + "]";
	}
	
} 

@Controller
public class AddController {
	
	@RequestMapping("/add")
	public ModelAndView add() {

		try{  
			Class.forName("com.mysql.cj.jdbc.Driver");  
			Connection con=DriverManager.getConnection(  
					"jdbc:mysql://localhost:3306/test","root","");  
			//here sonoo is database name, root is username and password  
			Statement stmt=con.createStatement();
			//			stmt.executeUpdate("insert into emp VALUES (null, 'shailesh', 31, 'Nagpur')");
			ResultSet rs=stmt.executeQuery("select * from test");  
			while(rs.next())
				System.out.println(rs.getString(1)+"  "+rs.getString(2)+"  "+rs.getString(3)+"  "+rs.getString(4));  
			con.close();  
		} catch(Exception e){ System.out.println(e);}

		ArrayList list = new ArrayList();
		
		list.add(5);
		list.add(6);
		

		Session session = new Configuration().configure("hibernate.cfg.xml").buildSessionFactory().openSession();
		Query query = session.createNativeQuery("select * from test");
		List list2 = query.list();
		
		
		ModelAndView mv = new ModelAndView("index.jsp");
		System.out.println("I am in controller");
		mv.addObject("num", list);
		return mv;
	}
	
	@ResponseBody
	@RequestMapping("ws")
	public List<Student> ws() {
		List<Student> list = new ArrayList<>();
		list.add(new Student("Shailesh"));
		list.add(new Student("Mahesh"));
		return list;
	}
}
